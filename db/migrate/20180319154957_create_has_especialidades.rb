class CreateHasEspecialidades < ActiveRecord::Migration[5.1]
  def change
    create_table :has_especialidades do |t|
      t.references :point, foreign_key: true
      t.references :especialidade, foreign_key: true

      t.timestamps
    end
  end
end
