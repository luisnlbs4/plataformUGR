class AddNewValuesToPoints < ActiveRecord::Migration[5.1]
  def change
    add_column :points, :nombre, :string
    add_column :points, :encargado, :string
    add_column :points, :cargo, :string
    add_column :points, :telefono_emergencia, :string
    add_column :points, :telefono_informaciones, :string
    add_column :points, :direccion, :string
  end
end
