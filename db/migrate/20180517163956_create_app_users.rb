class CreateAppUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :app_users do |t|
      t.string :nombre
      t.string :institucion
      t.string :email
      t.string :password
      t.string :cargo

      t.timestamps
    end
  end
end
