class CreateHasEmergencia < ActiveRecord::Migration[5.1]
  def change
    create_table :has_emergencia do |t|
      t.references :point, foreign_key: true
      t.references :emergency, foreign_key: true

      t.timestamps
    end
  end
end
