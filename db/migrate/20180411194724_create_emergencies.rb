class CreateEmergencies < ActiveRecord::Migration[5.1]
  def change
    create_table :emergencies do |t|
      t.string :name
      t.timestamps
    end
    drop_table :emergencia
  end
end
