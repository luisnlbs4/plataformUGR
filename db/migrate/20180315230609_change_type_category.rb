class ChangeTypeCategory < ActiveRecord::Migration[5.1]
  def up
    change_table :points do |t|
      t.change :latitude, :string
      t.change :longitude, :string
    end
  end

  def down
    change_table :points do |t|
      t.change :latitude, :float
      t.change :longitude, :float
    end
  end
end
