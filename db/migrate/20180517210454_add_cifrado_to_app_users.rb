class AddCifradoToAppUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :app_users, :cifrado, :string
  end
end
