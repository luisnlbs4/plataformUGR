class AddDescriptionToPoints < ActiveRecord::Migration[5.1]
  def change
    add_column :points, :description, :text
  end
end
