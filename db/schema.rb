# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180627163851) do

  create_table "app_users", force: :cascade do |t|
    t.string "nombre"
    t.string "institucion"
    t.string "email"
    t.string "password"
    t.string "cargo"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "cifrado"
  end

  create_table "emergencies", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "especialidades", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "has_emergencia", force: :cascade do |t|
    t.integer "point_id"
    t.integer "emergency_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["emergency_id"], name: "index_has_emergencia_on_emergency_id"
    t.index ["point_id"], name: "index_has_emergencia_on_point_id"
  end

  create_table "has_especialidades", force: :cascade do |t|
    t.integer "point_id"
    t.integer "especialidade_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["especialidade_id"], name: "index_has_especialidades_on_especialidade_id"
    t.index ["point_id"], name: "index_has_especialidades_on_point_id"
  end

  create_table "points", force: :cascade do |t|
    t.string "latitude"
    t.string "longitude"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "nombre"
    t.string "encargado"
    t.string "cargo"
    t.string "telefono_emergencia"
    t.string "telefono_informaciones"
    t.string "direccion"
    t.string "tipo"
    t.string "nivel"
    t.text "description"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "firstname"
    t.string "lastname"
    t.string "role"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

end
