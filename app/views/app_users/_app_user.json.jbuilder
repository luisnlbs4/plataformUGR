json.extract! app_user, :id, :nombre, :institucion, :email, :cargo, :created_at, :updated_at
json.url app_user_url(app_user, format: :json)
