json.extract! point, :id, :latitude, :longitude, :created_at, :updated_at
json.url point_url(point, format: :json)
