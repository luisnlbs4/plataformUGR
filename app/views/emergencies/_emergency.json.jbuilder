json.extract! emergency, :id, :name, :created_at, :updated_at
json.url emergency_url(emergency, format: :json)
