class HasEmergencia < ApplicationRecord
  belongs_to :point
  belongs_to :emergency
end
