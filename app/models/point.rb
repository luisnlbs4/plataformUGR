class Point < ApplicationRecord
  has_many :has_emergencias
  has_many :emergencies, through: :has_emergencias
  has_many :has_especialidades
  has_many :especialidades, through: :has_especialidades

end
