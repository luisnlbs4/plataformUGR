class Emergency < ApplicationRecord
  has_many :has_emergencias
  has_many :points, through: :has_emergencias
end
