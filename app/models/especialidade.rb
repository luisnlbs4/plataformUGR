class Especialidade < ApplicationRecord
  has_many :has_especialidades
  has_many :points, through: :has_especialidades
end
