require 'digest/md5'

class AppUsersController < ApplicationController
  before_action :set_app_user, only: [:show, :edit, :update, :destroy]
  # GET /app_users
  # GET /app_users.json
  def index
    @app_users = AppUser.order("nombre ASC")
  end

  # GET /app_users/1
  # GET /app_users/1.json
  def show
  end

  # GET /app_users/new
  def new
    @app_user = AppUser.new
  end

  # GET /app_users/1/edit
  def edit
  end

  # POST /app_users
  # POST /app_users.json
  def create
    @app_user = AppUser.new(app_user_params)
    @app_user.password = Digest::MD5.hexdigest(@app_user.password)
    @app_user.cifrado = Digest::MD5.hexdigest(@app_user.password+@app_user.email)
      if @app_user.save
          redirect_to "/app_users/", notice: "usuario creado"
      else
        render 'new'
    end
  end

  # PATCH/PUT /app_users/1
  # PATCH/PUT /app_users/1.json
  def update

      if @app_user.update(app_user_params)
        @app_user.password = Digest::MD5.hexdigest(@app_user.password)
        @app_user.cifrado = Digest::MD5.hexdigest(@app_user.password+@app_user.email)
        @app_user.save
        redirect_to "/app_users/", notice: "usuario creado"
      else
        redirect_to "/app_users/<%=@app_user.id%>/edit", notice: "usuario creado"
      end
    end

  # DELETE /app_users/1
  # DELETE /app_users/1.json
  def destroy
    @app_user.destroy
    respond_to do |format|
      format.html { redirect_to app_users_url, notice: 'App user was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_app_user
      @app_user = AppUser.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def app_user_params
      params.require(:app_user).permit(:nombre, :institucion, :email, :cargo, :password)
    end
end
