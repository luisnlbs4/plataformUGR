class UsuariosController < ApplicationController


  def index
  	@users = User.order("email ASC")
  end

  def new
  	@user = User.new
  end

  def create
  	@user = User.new(user_params)
		if @user.save
			redirect_to "/usuarios", notice: "Usuario creado exitosamente"
		else
			redirect_to "/usuarios/new", notice: "Error al crear usuario"
		end
  end

  def edit
    @user = User.find(params[:id])
  end

  def update_user
    @user = User.find(params[:id])
    if @user.update_attributes(user_params)
      redirect_to "/usuarios/", notice: "Contraseña cambiada exitosamente"
    else
      redirect_to "/user/"+@user.id.to_s+"/edit_user/", notice: "Error al cambiar Contraseña"
    end
  end


  def user_params
	params.require(:user).permit(:email,:password,:password_confirmation,:role,:firstname,:lastname)
  end


end
