class WelcomeController < ApplicationController
  def index
    @points = Point.all
    respond_to do |format|
      format.html {render :index}
      format.json {render json: @points}
    end
  end
   #json_response(@points)
end
