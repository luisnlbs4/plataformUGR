module Api
  module V1
    class PointsController < ApplicationController
      def index
         @points = Point.order('nombre ASC');
         @points.each do |point|
           render json: {status: 'SUCCESS' , message: 'Loaded points', data: @points}, status: :ok
           return true
         end
      end

      def especialidades
        @points = Point.find(params[:id]);
        @especialidades = @points.especialidades.order('name ASC')
        render json: {status: 'SUCCESS' , message: 'Loaded points', data: @especialidades}, status: :ok
        return true
      end
      def getAllespecialidades
        @especialidadesAll = Especialidade.order('name ASC')
        render json: {status: 'SUCCESS' , message: 'Loaded points', data: @especialidadesAll}, status: :ok
        return true
      end
      def getAllCentrosofEspecialidad
        @especialidade = Especialidade.find(params[:id]);
        @result = @especialidade.points.order('nombre ASC')
        render json: {status: 'SUCCESS' , message: 'Loaded points', data: @result}, status: :ok
        return true
      end

      def getAllemergencias
        @especialidadesAll = Emergency.order('name ASC')
        render json: {status: 'SUCCESS' , message: 'Loaded points', data: @especialidadesAll}, status: :ok
        return true
      end
      def getAllCentrosofEmergencia
        @especialidade = Emergency.find(params[:id]);
        @result = @especialidade.points.order('nombre ASC')
        render json: {status: 'SUCCESS' , message: 'Loaded points', data: @result}, status: :ok
        return true
      end
      def getUser
        @user = AppUser.order('email ASC')
        render json: {status: 'SUCCESS' , message: 'Loaded points', data:  @user }, status: :ok
        return true
      end

    end
  end
end
