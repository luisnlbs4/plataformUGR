class PointsController < ApplicationController
  before_action :set_point, only: [:show, :edit, :update, :destroy]

  # GET /points
  # GET /points.json
  def index
    @points = Point.all
    respond_to do |format|
      format.html {render :index}
      format.json {render json: @points}
    end
   #json_response(@points)
  end

  # GET /points/1
  # GET /points/1.json
  def show
  end

  def list
    @nombre = params[:nombre]
    @sort = params[:sort]
    if @nombre
      if @sort
         @specialidades = Point.where("nombre like ?", "%#{@nombre}%")
         @points2 = @specialidades.order('nombre ASC')
         @points = @points2.order(params[:sort])
      else
        @specialidades = Point.where("nombre like ?", "%#{@nombre}%")
        @points = @specialidades.order('nombre ASC')
      end
    else
      if @sort
         @points = Point.order(params[:sort])
      else
        @points = Point.order('nombre ASC')
      end
    end
  end
  def enlazar
    @especialidades = Especialidade.order('name ASC')
    @point = Point.find(params[:id])
    @especialidadesP = @point.especialidades.order('name ASC')
    @estado = true
  end

  def enlazarEmergencias
    @especialidades = Emergency.order('name ASC')
    @point = Point.find(params[:id])
    @especialidadesP = @point.emergencies.order('name ASC')
    @estado = true
  end

  def cenlazar
    @point = Point.find(params[:id])
    @especialidades = params[:especialidades]
    HasEspecialidade.where(:point_id => @point.id).destroy_all
    @especialidades.each do |especialidad_id|
      HasEspecialidade.create(especialidade_id: especialidad_id, point_id: @point.id)
    end
    redirect_to("/list/points")
  end
  def cenlazarEmergencias
    @point = Point.find(params[:id])
    @especialidades = params[:especialidades]
    HasEmergencia.where(:point_id => @point.id).destroy_all
    @especialidades.each do |especialidad_id|
      HasEmergencia.create(emergency_id: especialidad_id, point_id: @point.id)
    end
    redirect_to("/list/points")
  end

  # GET /points/new
  def new
    @point = Point.new
  end

  # GET /points/1/edit
  def edit
  end

  # POST /points
  # POST /points.json
  def create
    @point = Point.new(point_params)
    respond_to do |format|
      if @point.save
        format.html { redirect_to("/list/points")}
        format.json { render :show, status: :created, location: @point }
      else
        format.html { render :new }
        format.json { render json: @point.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /points/1
  # PATCH/PUT /points/1.json
  def update
    respond_to do |format|
      if @point.update(point_params)
        format.html { redirect_to("/list/points")}
        format.json { render :show, status: :ok, location: @point }
      else
        format.html { render :edit }
        format.json { render json: @point.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /points/1
  # DELETE /points/1.json
  def destroy
    HasEspecialidade.where(:point_id => @point.id).destroy_all
    @point.destroy
    respond_to do |format|
      format.html { redirect_to("/list/points")}
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_point
      @point = Point.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def point_params
      params.require(:point).permit(:latitude, :longitude,:nombre,:encargado,:cargo,:telefono_emergencia,:telefono_informaciones,:direccion,:tipo,:description)
    end
end
