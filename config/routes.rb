Rails.application.routes.draw do
  resources :app_users
  resources :emergencies
  resources :especialidades
  root 'welcome#index'

  devise_for :users
  resources :points
  get '/calculate/' => 'route#calculate'
  get '/usuarios' => 'usuarios#index'
  get '/usuarios/new' => 'usuarios#new'
  post '/usuarios' => 'usuarios#create'
  get '/user/:id/edit_user' => 'usuarios#edit'
  post '/user/:id/update_user' => 'usuarios#update_user'
  get '/enlazar/:id' => 'points#enlazar'
  get '/enlazarEmergencias/:id' => 'points#enlazarEmergencias'
  post 'create/enlazar/:id' => 'points#cenlazar'
  post 'create/enlazarEmergencias/:id' => 'points#cenlazarEmergencias'
  get '/list/points' => 'points#list'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html


  namespace 'api' do
    namespace 'v1' do
       resources :points
       get '/points/get/:id' => 'points#especialidades'
       get '/especialidades' => 'points#getAllespecialidades'
       get '/especialidades/:id' => 'points#getAllCentrosofEspecialidad'
       get '/emergencias' => 'points#getAllemergencias'
       get '/emergencias/:id' => 'points#getAllCentrosofEmergencia'
       get '/user' => 'points#getUser'

    end
  end
end
